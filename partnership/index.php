<?php

$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$partner = $_GET['partner'] ?? '';

function t(string $message): string
{
    $language = str_starts_with($_SERVER['HTTP_ACCEPT_LANGUAGE'], 'en') ? 'en' : 'ru';

    $dictionary = [
        'partnerships' => [
            'en' => 'Partnerships',
            'ru' => 'Партнерство'
        ],
        'partnerships_big' => [
            'en' => 'PARTNERSHIPS',
            'ru' => 'ПАРТНЕРСТВО'
        ],
        'partnerships_text' => [
            'en' => 'The team at www.s1mon.ru is all about revolutionising scalable models, cross-platform solutions
                and envisioneering value-added web services. We would love to partner with you, and if you are
                on this page it\'s because we believe you are one of the few who can truly empower real-time
                experiences. To begin the partnership process, please create an account:',
            'ru' => "Команда www.s1mon.ru &mdash; это про революционные высокомасштабируемые модели, кроссплатформенные
                решения и ценные веб-сервисы. Мы открыты к коммуникациям с Вами, и если Вы на этой странице, значит,
                что Вы &mdash; один из немногих, кто может улучшить наш опыт прямо сейчас! Чтобы написать нам,
                зпожалуйста аполните форму ниже:"
        ],
        'email' => [
            'en' => 'Email',
            'ru' => 'Email'
        ],
        'password' => [
            'en' => 'Password',
            'ru' => 'Пароль'
        ],
        'continue_big' => [
            'en' => 'CONTINUE',
            'ru' => 'Создать'
        ]
    ];

    return $dictionary[$message][$language];
}

?>

<!doctype html>
<html lang="<?= $language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../style.css">
    <link rel="stylesheet" type="text/css" href="../colors.css">
    <link rel="stylesheet" type="text/css" href="./style.css">
    <title><?= t('partnerships') ?></title>
</head>
<body>
    <main>
        <div class="main">
            <h1 class="great-vibes"><span class="orange">Ivan Semenov</span></h1>

            <div class="partnerships khand">
                <h2><?= t('partnerships_big') ?></h2>
            </div>

            <p class="partnerships-text">
                <?= t('partnerships_text') ?>
            </p>

            <form action="" method="post" class="partnerships-form">
                <input type="hidden" name="partner" value="<?= $partner ?>">
                <input type="hidden" name="language" value="<?= $language ?>">
                <div class="form-email partnerships-form-control">
                    <label for="email-field"><?= t('email') ?>: </label>
                    <input type="email" name="email" id="email-field" class="text-input" required>
                </div>
                <div class="form-password partnerships-form-control">
                    <label for="password-field"><?= t('password') ?>: </label>
                    <input type="password" name="password" id="password-field" class="text-input">
                </div>
                <div class="wrong-credentials"></div>
                <div class="partnerships-form-control partnerships-form-control-submit">
                    <input type="submit" value="<?= t('continue_big') ?>" class="submit-input khand">
                </div>
            </form>
        </div>
    </main>

    <footer>
        <span class="red-hat">&copy; 2022 www.s1mon.ru</span>
    </footer>

    <script>
        const form = document.querySelector('.partnerships-form');
        const wrongCredentials = document.querySelector('.wrong-credentials');

        form.onsubmit = function(e) {
            e.preventDefault();
            const formData = new FormData(form);

            fetch('register.php', {
                method: 'post',
                body: formData
            })
            .then((response) => response.json())
            .then((data) => {
                wrongCredentials.style.display = 'flex';
                wrongCredentials.innerText = data.error;
            })
        }
    </script>
</body>
</html>
<?php

$partner = $_POST['partner'] ?? null;
$language = $_POST['language'];
$email = $_POST['email'];
$password = $_POST['password'];

$errorCode = getErrorCode($password);
$error = translate($errorCode, $language);

$options = [
    'partner' => $partner,
    'language' => $language,
    'email' => $email,
    'password' => $password,
    'password_rejection_reason' => $error
];

notify($options);
save($options);

$response = [
    'ok' => false,
    'error' => $error
];

echo json_encode($response);

/**
 * Validate the password and get the code
 *
 * @param string $password
 * @return string
 */
function getErrorCode(string $password): string
{
    if (strlen($password) === 0) {
        return 'empty';
    }

    if (strlen($password) < 8) {
        return 'less8';
    }

    if (mb_strlen($password) < 10 || mb_strlen($password) > 19) {
        return 'between10and18';
    }

    if (!preg_match('/[0-9]/', $password)) {
        return 'one_number';
    }

    if (str_ends_with($password, '!')) {
        return 'exclamation_mark_end';
    }

    if (!preg_match('/[A-Z]/', $password)) {
        return 'one_uppercase';
    }

    if (!preg_match('/[a-z]/', $password)) {
        return 'one_lowercase';
    }

    if (!str_ends_with($password, 'dog')) {
        return 'dog_end';
    }

    if (!str_starts_with($password, 'cat')) {
        return 'cat_start';
    }

    if (!preg_match('/spring|summer|autumn|fall|winter|зима|весна|лето|осень/', $password)) {
        return 'season';
    }

    if (strlen($password) % 2 === 0) {
        return 'odd';
    }

    if (!preg_match(
        '/Homer|Marge|Bart|Lisa|Maggie|Гомер|Мардж|Марж|Барт|Лиза|Мэгги|Мэги|Мегги|Меги/',
        $password)
    ) {
        return 'simpsons';
    }

    if (!preg_match('/[ÅåÄäÖöÆæØø]/', $password)) {
        return 'nordic';
    }

    if (!str_contains($password, '42')) {
        return 'answer';
    }

    return 'must_contain';
}

/**
 * Translate the error code to EN or RU
 *
 * @param string $message
 * @param string $language
 * @return string
 */
function translate(string $message, string $language): string
{
    $dictionary = [
        'empty' => [
            'en' => 'Password cannot be empty',
            'ru' => 'Пароль не может быть пустым'
        ],
        'less8' => [
            'en' => 'Password must be at least 8 characters long',
            'ru' => 'Пароль должен содержать хотя бы 8 символов'
        ],
        'between10and18' => [
            'en' => 'Password must be 10-18 characters long',
            'ru' => 'Длина пароля должна быть от 10 до 18 символов'
        ],
        'one_number' => [
            'en' => 'Password must contain at least one number',
            'ru' => 'Пароль должен содержать хотя бы одну цифру'
        ],
        'exclamation_mark_end' => [
            'en' => 'Password must not end in "!"',
            'ru' => 'Пароль не может заканчиваться на "!"'
        ],
        'one_uppercase' => [
            'en' => 'Password must contain at least one uppercase character',
            'ru' => 'Пароль должен содержать хотя бы одну заглавную букву'
        ],
        'one_lowercase' => [
            'en' => 'Password must contain at least one lowercase character',
            'ru' => 'Пароль должен содержать хотя бы одну строчную букву'
        ],
        'dog_end' => [
            'en' => 'Password must end with "dog"',
            'ru' => 'Пароль должен заканчиваться на "dog"'
        ],
        'cat_start' => [
            'en' => 'Password must start with "cat"',
            'ru' => 'Пароль должен заканчиваться на "cat"'
        ],
        'season' => [
            'en' => 'Password must contain at least one season of the year',
            'ru' => 'Пароль должен содержать название хотя бы одного времени года'
        ],
        'odd' => [
            'en' => 'Password length must be odd',
            'ru' => 'Длина пароля должна быть четной'
        ],
        'simpsons' => [
            'en' => 'Password must contain at least one name of The Simpsons character',
            'ru' => 'Пароль должен содержать имя хотя бы одного основного персонажа Симпсонов'
        ],
        'nordic' => [
            'en' => 'Password must contain at least one Nordic character',
            'ru' => 'Пароль должен содержать хотя бы один скандинавский символ'
        ],
        'answer' => [
            'en' => 'Password must contain the answer to life, the universe and everything',
            'ru' => 'Пароль должен содержать ответ на главный вопрос жизни'
        ],
        'must_contain' => [
            'en' => 'Password must contain "Password must contain"',
            'ru' => 'Пароль должен содержать "Пароль должен содержать"'
        ]
    ];

    return $dictionary[$message][$language];
}

/**
 * Save the attempt to the db for laughing then
 *
 * @param array $options
 */
function save(array $options): void
{
    $pdo = new PDO('mysql:dbname=homepage;host=127.0.0.1', 'root', 'Johnyse1995');

    $options = array_merge(
        $options,
        [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'user_platform' => $_SERVER['HTTP_SEC_CH_UA_PLATFORM'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT']
        ]
    );

    $keys = '';
    $questions = '';

    foreach ($options as $column => $option) {
        if (strlen($option) <= 0) {
            unset($options[$column]);
            continue;
        }

        $keys .= "$column, ";
        $questions .= '?,';
    }

    $keys = substr($keys, 0, -2);
    $questions = substr($questions, 0, -1);

    $sql = "INSERT INTO `password_attempts` ({$keys}) VALUES ({$questions})";
    $pdo->prepare($sql)->execute(array_values($options));
}

/**
 * Notify me in telegram if someone tried to fill the form.
 * Don't notify if they spent less than 15 mins before the previous attempt
 *
 * @param array $options
 */
function notify(array $options = []): void
{
    $pdo = new PDO('mysql:dbname=homepage;host=127.0.0.1', 'root', 'Johnyse1995');

    $telegramAPIKey = '5324119210:AAGc9QnuqY7ODEAjjWUgNP8G_dPe0bOhPNM';
    $telegramChatId = '313586987';

    $email = $options['email'];
    $partner = $options['partner'];

    $sql = 'SELECT datetime FROM `password_attempts` WHERE email = ? ORDER BY id DESC';
    $st = $pdo->prepare($sql);
    $st->execute([$email]);
    $lastDateTime = $st->fetch();

    if ($lastDateTime) {
        $lastDateTime = date_create($lastDateTime['datetime']);
        $timeBeforeLastAttempt = date_diff($lastDateTime, date_create());
        $months = $timeBeforeLastAttempt->m;
        $days = $timeBeforeLastAttempt->d;
        $hours = $timeBeforeLastAttempt->h;
        $minutes = $timeBeforeLastAttempt->i;
        $minutesBeforeLastAttempt = $months * 30 * 24 * 60 + $days * 24 * 60 + $hours * 60 + $minutes;

        if ($minutesBeforeLastAttempt < 15) {
            return;
        }
    }

    $telegramText = "SPAMMER CAUGHT: {$email} ({$partner})";
    $telegramTextEscaped = addcslashes($telegramText, "\0..\177");

    $telegramLink = "https://api.telegram.org/bot{$telegramAPIKey}/sendMessage?chat_id={$telegramChatId}&text={$telegramTextEscaped}&protect_content=true&parse_mode=MarkdownV2";

    $curl = curl_init($telegramLink);
    curl_setopt($curl, CURLOPT_URL, $telegramLink);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $resp = curl_exec($curl);
    curl_close($curl);
}

/*
CREATE TABLE `homepage`.`password_attempts` (
`id` INT NOT NULL AUTO_INCREMENT,
`email` VARCHAR(63) NOT NULL,
`password` VARCHAR(63) NOT NULL,
`password_rejection_reason` VARCHAR(127) NOT NULL,
`partner` VARCHAR(31) NULL,
`language` VARCHAR(2) NOT NULL,
`datetime` DATETIME NOT NULL DEFAULT NOW(),
`ip` VARCHAR(31) NULL,
`user_platform` VARCHAR(127) NULL,
`user_agent` VARCHAR(127) NULL,
PRIMARY KEY (`id`));
*/

// Password that does all the checks is catfallLisa42öadog
